import java.util.Random;
public c l a s s Al t Bi tP r o t o c ol {
public s t a t i c void main ( S t ri n g [ ] a r g s ) {
LossyChannel ch annel = new LossyChannel ( ) ;
(new Thread (new Sender ( ch annel ) ) ) . s t a r t ( ) ;
(new Thread (new R e c ei v e r ( ch annel ) ) ) . s t a r t ( ) ;
}
}
c l a s s LossyChannel {
private boolean p r o t o c ol Bi t , channelEmpty = true , ac kBi t = true ;
private Random r ;
public LossyChannel ( ) { r = new Random ( ) ; }
public synchronized boolean getAckBi t ( ) { n o t i f y ( ) ; return ac kBi t ; }
public synchronized void putAckBit (boolean ac kBi t ) {
th is . ac kBi t = ac kBi t ;
int i g n o r e Bi t = r . n e x t I n t ( 2 ) ;
i f ( i g n o r e Bi t > 0 ) { th is . ac kBi t = ! th is . ac kBi t ; } n o t i f y ( ) ;
}
public synchronized boolean g e t ( ) {
while ( ! channelEmpty ) {try { w ai t ( ) ; } catch ( I n t e r r u p t e dE x c e p ti o n e ) { } }
channelEmpty = true ; n o t i f y ( ) ; return p r o t o c ol B i t ;
}
public synchronized void put (boolean p r o t o c ol B i t ) {
while ( ! channelEmpty ) {try { w ai t ( ) ; } catch ( I n t e r r u p t e dE x c e p ti o n e ) { } }
int i g n o r e Bi t = r . n e x t I n t ( 2 ) ;
channelEmpty = f a l s e ; i f ( i g n o r e Bi t > 0 ) channelEmpty = true ;
n o t i f y ( ) ;
}
}
c l a s s Sender implements Runnable {
private LossyChannel ch annel ;
public Sender ( LossyChannel ch annel ) { th is . ch annel = ch annel ; }
public void run ( ) {
boolean p r o t o c ol B i t = f a l s e ; Random r = new Random ( ) ;

while ( true ) {
i f ( p r o t o c o l Bi t != ch annel . ge tAckBit ( ) ) ch annel . put ( p r o t o c o l Bi t ) ;
e l s e p r o t o c ol B i t = ! p r o t o c ol B i t ;
try {Thread . s l e e p ( r . n e x t I n t ( 1 5 0 0 ) ) ; } catch ( I n t e r r u p t e dE x c e p ti o n e ) { }
}
}
}
c l a s s R e c ei v e r implements Runnable {
private LossyChannel ch annel ;
public R e c ei v e r ( LossyChannel ch annel ) { th is . ch annel = ch annel ; }
public void run ( ) {
Random r = new Random ( ) ;
while ( true ) {
boolean p r o t o c ol B i t = ch annel . g e t ( ) ; ch annel . putAckBit ( p r o t o c ol B i t ) ;
try {Thread . s l e e p ( r . n e x t I n t ( 1 5 0 0 ) ) ; } catch ( I n t e r r u p t e dE x c e p ti o n e ) { }
}
}
}